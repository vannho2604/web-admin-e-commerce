import React, { useState, useEffect } from "react";

import EditProduct from "../pages/Login/EditProduct.jsx";
import ProductDetail from "./ProductDetail";

import axios from "axios";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Product({
  id,
  img,
  name,
  description,
  price,
  setValue,
}) {
  const [editmodalOpen, setEditmodalOpen] = useState(false);
  const [addmodalOpen, setAddmodalOpen] = useState(false);
  if (addmodalOpen) {
    document.documentElement.style.overflow = "hidden";
  } else {
    document.documentElement.style.overflow = "auto";
  }
  const setModal = () => {
    setEditmodalOpen(!editmodalOpen);
    setAddmodalOpen(false)
  }
  return (
    <>
    <div className="product-box">
      {addmodalOpen ? (
        <ProductDetail
          img={img}
          name={name}
          price={price}
          description={description}
          setOpenModal={setAddmodalOpen}
        />
      ) : null}
      <div className="wrapper" onClick={() => setAddmodalOpen(!addmodalOpen)}>
        <div className="product-img">
          <img src={img} alt="kh"/>
        </div>
        <div className="product-description">
          <h5>{name}</h5>
        </div>
        <div className="product-price">
          <h4>{price}</h4>
        </div>
      </div>
      <div className="product-action">
        <div className="edit-action">
          <button onClick={() => setModal()}>
            <i
              className="bx bx-edit"
              style={{ fontSize: "20px", lineHeight: 1.5 }}
            />
          </button>
        </div>
        <div className="delete-action">
          <button>
            <i
              className="bx bx-trash"
              style={{ fontSize: "20px", lineHeight: 1.5 }}
            />
          </button>
        </div>
      </div>
    </div>
      {editmodalOpen ? (
        <EditProduct
          setOpenModal={setEditmodalOpen}
          setAddmodalOpen={setAddmodalOpen}
          idProduct={id}
          setValue={setValue}
        />
      ) : null}
    </>
  );
}
