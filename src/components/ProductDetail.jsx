import React, { useState } from "react";
import "./ProductDetail.css";
export default function ProductDetail({ img, name, description, price, setOpenModal }) {
  return (
    <div className="detail-product">
      <div className="detail-product_layout">
            <span
              className="close-icon"
              onClick={() => {
                setOpenModal(false);
              }}
            >
              &times;
            </span>
        <div className="product-detail__top">
          <div className="product-detail-top-left">
            <img src={img} alt="" className="product-detail-top-left__img" />
          </div>
          <div className="product-detail-top-right">
            <h2 className="product-detail__name">{name}</h2>
            <p className="product-detail__descriptiont">{description}</p>
            <p className="product-detail__price">Price: {price}</p>
            <div className="product-detail__stock-group">
              <p>
              Availability: <span>Instock</span>
              </p>
              <p>
              Size: <span>30</span>
              </p>
            </div>
            <div className="product-detail__button-group">
              {/* <button>Edit</button>
              <button>Delete</button> */}
            </div>
          </div>
        </div>
        <div className="product-detail__bot">
          <h2>Description</h2>
          <p className="product-detail__descriptiont">{description}</p>
        </div>
      </div>
    </div>
  );
}
