import React from 'react'

import "./modal.css";

function AddadminModal({ setOpenModal }) {
    return (
      <div className="modalBackground">
        <div className="modalContainer">
          <span
            className="close-icon"
            onClick={() => {
              setOpenModal(false);
            }}
          >
            &times;
          </span>
          <h2 style={{ color: "#202020" }}>Add New Category</h2>
          <input type="text" placeholder="Name" className="input-text" />
          <input type="text" placeholder="Email" className="input-text" />
  
          <div className="modalFooter">
            <button
              className="buton-add"
              onClick={() => {
                setOpenModal(false);
              }}
            >
              Add
            </button>
            <button
              className="button-delete"
              onClick={() => {
                setOpenModal(false);
              }}
              id="cancelBtn"
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    );
  }

export default AddadminModal
