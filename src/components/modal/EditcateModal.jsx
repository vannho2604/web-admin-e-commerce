import React from "react";

import "./modal.css";

function EditcateModal({ setOpenModal }) {
  return (
    <div className="modalBackground">
      <div className="modalContainer">
        <span
          className="close-icon"
          onClick={() => {
            setOpenModal(false);
          }}
        >
          &times;
        </span>
        <h2 style={{color:"#202020"}}>Edit Category</h2>
        <input type="text" placeholder="Name" className="input-text" />

        <div className="modalFooter">
          <button
            className="button-edit"
            onClick={() => {
              setOpenModal(false);
            }}
          >
            Edit
          </button>
          <button
            className="button-delete"
            onClick={() => {
              setOpenModal(false);
            }}
            id="cancelBtn"
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
}

export default EditcateModal;
