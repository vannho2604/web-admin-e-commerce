const Row = ({ item, value, onEdit = () => {}, onDelete = () => {}, ...props }) => {
  //         // const [editmodalOpen, seteditmodalOpen] = useState(false)

  return (
    <tr>
      {/* <td>{item.id}</td> */}
      <td>{item.category_id}</td>
      <td>{item.name}</td>
      <td>
        <div className="cate-action">
          <div className="edit-action">
            <button onClick={onEdit}>
              <i className="bx bx-edit" style={{ fontSize: "20px",lineHeight: 1.5}} />
            </button>
            {/* {editmodalOpen ? <EditcateModal setOpenModal={seteditmodalOpen}/>:null}                                      */}
            <div className="modal-layout-edit"></div>
          </div>
          <div className="delete-action">
            <button onClick={onDelete}>
              <i className="bx bx-trash" style={{ fontSize: "20px",lineHeight: 1.5}} />
            </button>
          </div>
        </div>
      </td>
    </tr>
  );
};

export default Row;
