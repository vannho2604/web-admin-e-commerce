import React from "react";

import { Route, Switch, BrowserRouter as Router } from "react-router-dom";

import Dashboard from "../pages/Dashboard";
import Users from "../pages/Users";
import Orders from "../pages/Orders";
import Categories from "../pages/Categories";
import Products from "../pages/Products";
import Order_Detail from "../pages/Order_Detail";
import Saleman_Revenue from "../pages/Saleman_Revenue";
import Admin from "../pages/Admin";
import Kpi from "../pages/Kpi";
import Kpi_Detail from "../pages/Kpi_Detail";
import Checkin from "../pages/Checkin";
import AuthContextProvider from "../context/AuthContext";
import Layout from "./layout/Layout";
import Login from "../pages/Login/Login";

const Routes = () => {
  return (    
        <Switch>
          <Route path="/users" component={Users} />
          <Route path="/orders" component={Orders} />
          <Route path="/categories" component={Categories} />
          <Route path="/products" component={Products} />
          <Route path="/order_detail" component={Order_Detail} />
          <Route path="/saleman_revenue" component={Saleman_Revenue} />
          <Route path="/kpi" component={Kpi} />
          <Route path="/kpi_detail" component={Kpi_Detail} />
          <Route path="/checkin" component={Checkin} />
          <Route path="/admin" component={Admin} />
          <Route path="/" exact component={Dashboard} />
         
          {/* <Route path="/login" component={Login} /> */}
          {/* <Route path="/"  component={Layout} /> */}
        </Switch>      
  );
};

export default Routes;
