import React, { useState, useEffect } from "react";
import axios from "axios";
import ProductItem from "../components/productItem.jsx";
import { Dropdown } from "react-bootstrap";
import AddProduct from "./Login/AddProduct.jsx";
import { apiUrl } from "../context/Constants";

import Pagination from "../components/Pagination/index.jsx";

const Products = () => {
  const [addmodalOpen, setAddmodalOpen] = useState(false);
  // const [pagination, setPagination] = useState({
  //   _page: 1,
  //   _limit: 10,
  //   _totalRows: 11,
  // });
  const [products, setProducts] = useState();
  const [value, setValue] = useState(false);
  // const [filters, serFilters] = useState({
  //   _limit: 10,
  //   _page: 1,
  // });

  // function handlePageChange(newPage) {
  //   console.log("New Page:", newPage);
  // }

  async function fetchData() {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${localStorage["token"]}`;
    const response = await axios.get(`${apiUrl}/product/list`);
    setProducts(response.data.data);
    setValue(false);
  }

  useEffect(() => {
    fetchData();
  }, [value]);
  return (
    <div>
      <h2 className="page-header">Products</h2>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card__header">
              <div>
                <Dropdown>
                  <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select all categories
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/">Hai Chau</Dropdown.Item>
                    <Dropdown.Item href="/">Thanh Khe</Dropdown.Item>
                    <Dropdown.Item href="/">Son Tra</Dropdown.Item>
                    <Dropdown.Item href="/">Ngu Hanh Son</Dropdown.Item>
                    <Dropdown.Item href="/">Lien Chieu</Dropdown.Item>
                    <Dropdown.Item href="/">Cam Le</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
              <div>
                <Dropdown>
                  <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Default sorting
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/">Hai Chau</Dropdown.Item>
                    <Dropdown.Item href="/">Thanh Khe</Dropdown.Item>
                    <Dropdown.Item href="/">Son Tra</Dropdown.Item>
                    <Dropdown.Item href="/">Ngu Hanh Son</Dropdown.Item>
                    <Dropdown.Item href="/">Lien Chieu</Dropdown.Item>
                    <Dropdown.Item href="/">Cam Le</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
              <div className="add-new-cate">
                <button onClick={() => setAddmodalOpen(!addmodalOpen)}>
                  Add New Product
                </button>
                {addmodalOpen ? (
                  <AddProduct
                    setOpenModal={setAddmodalOpen}
                    setValue={setValue}
                  />
                ) : null}
              </div>
            </div>
            <div className="product-panel">
              {products?.slice(0, 8).map((item, index) => {
                return (
                  <ProductItem
                    key={index}
                    id={item.id}
                    img={item.image}
                    name={item.title}
                    price={item.price}
                    description={item.description}
                    setValue={setValue}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
      {/* <Pagination pagination={pagination} onPageChange={handlePageChange} /> */}
    </div>
  );
};

export default Products;
