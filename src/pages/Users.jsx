import React from "react";

import Table from "../components/table/Table";

import userList from "../assets/JsonData/users-list.json";

import { Dropdown } from "react-bootstrap";

// import { withRouter } from "react-router-dom";

const userTableHead = [
  "ID",
  "name",
  "email",
  "phone",
  "address",
  "area",
  "utype",
  "created_at",
  "confirm",
  "action",
];

const renderHead = (item, index) => <th key={index}>{item}</th>;

const renderBody = (item, index) => (
  <tr key={index}>
    <td>{item.id}</td>
    <td>{item.name}</td>
    <td>{item.email}</td>
    <td>{item.phone}</td>
    <td>{item.address}</td>
    {item.utype === "Saleman" ? (
      // <div className="custom-select" style={{"width":"130px"}}>
        <select>
          <option value="0">Select area:</option>
          <option value="1">Hai Chau</option>
          <option value="2">Thanh Khe</option>
          <option value="3">Son Tra</option>
          <option value="4">Cam Le</option>
          <option value="5">Lien Chieu</option>
          <option value="6">Ngu Hanh Son</option>
        </select>
      // </div>
    ) : (
      <td></td>
    )}
    <td>{item.utype}</td>
    <td>{item.created_at}</td>
    <td>{item.enable ? <div style={{"color": "green"}}>Confirmed</div> : <div className="confirm"><button >Confirm</button></div> }</td>
    <td>
      <div className="delete-action">
        {item.enable ? (
          <button>
            <i
              className="bx bx-trash"
              style={{ fontSize: "20px", lineHeight: 1.5 }}
            />
          </button>
        ) : null}
      </div>
    </td>
  </tr>
);

const Users = () => {
  return (
    <div>
      <h2 className="page-header">users</h2>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card__header">
              <div>
                <Dropdown>
                  <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select all users
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/">Saleman</Dropdown.Item>
                    <Dropdown.Item href="/">Shop</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
              <div>
                <Dropdown>
                  <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select all areas
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/">Hai Chau</Dropdown.Item>
                    <Dropdown.Item href="/">Thanh Khe</Dropdown.Item>
                    <Dropdown.Item href="/">Son Tra</Dropdown.Item>
                    <Dropdown.Item href="/">Ngu Hanh Son</Dropdown.Item>
                    <Dropdown.Item href="/">Lien Chieu</Dropdown.Item>
                    <Dropdown.Item href="/">Cam Le</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
            <div className="card__body">
              <Table
                limit="10"
                headData={userTableHead}
                renderHead={(item, index) => renderHead(item, index)}
                bodyData={userList}
                renderBody={(item, index) => renderBody(item, index)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Users;
