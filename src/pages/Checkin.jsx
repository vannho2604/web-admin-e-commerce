import React from 'react'

import Table from '../components/table/Table'

import checkinList from '../assets/JsonData/checkin-list.json'

const checkinTableHead = [
    'ID',
    'name',
    'area'
]

const renderHead = (item, index) => <th key={index}>{item}</th>

const renderBody = (item, index) => {
    return(<tr key={index}>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.area}</td>        
    </tr>)
}
const Area_Active = () => {
    return (
        <div>
            <h2 className="page-header">
                Check in
            </h2>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="card__body">
                            <Table
                                limit='5'
                                headData={checkinTableHead}
                                renderHead={(item, index) => renderHead(item, index)}
                                bodyData={checkinList}
                                renderBody={(item, index) => renderBody(item, index)}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Area_Active
