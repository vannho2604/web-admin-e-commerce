import React, { useState } from "react";

import Table from '../components/table/Table'

import adminList from '../assets/JsonData/admin-list.json'

import AddadminModal from "../components/modal/AddadminModal";

const adminTableHead = [
    'ID',
    'name',
    'area'
]

const renderHead = (item, index) => <th key={index}>{item}</th>

const renderBody = (item, index) => {
    return(<tr key={index}>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.area}</td>        
    </tr>)
}
const Admin = () => {
  const [addmodalOpen, setaddmodalOpen] = useState(false);
    return (
        <>
            <div>
                <h2 className="page-header">
                    Admin
                </h2>
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card_header">
                                <div className="add-new-cate">                       
                                    <button
                                    onClick={() => {
                                        setaddmodalOpen(!addmodalOpen);
                                    }}
                                    >
                                        Add new admin
                                    </button>
                                </div>                                                     
                            </div>
                            <div className="card__body">
                                <Table
                                    limit='5'
                                    headData={adminTableHead}
                                    renderHead={(item, index) => renderHead(item, index)}
                                    bodyData={adminList}
                                    renderBody={(item, index) => renderBody(item, index)}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Admin
