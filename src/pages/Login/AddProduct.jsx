import React, { useEffect, useState } from "react";
import "../../components/modal/modal.css";
import "./AddProduct.css";
import upload from "../../assets/images/upload1.jpg";
import axios from "axios";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function AddProduct({ setOpenModal, setValue }) {
  const [userlist, setUserlist] = useState();
  const [cateList, setCatelist] = useState();

  async function fetchData() {
    const response = await axios.get(
      `http://localhost:8000/api/admin/user/shop/list`
    );
    setUserlist(response.data.data);

    const response1 = await axios.get(
      `http://localhost:8000/api/category/list`
    );
    setCatelist(response1.data.data);
  }

  useEffect(() => {
    fetchData();
  }, []);

  const [title, setTitle] = useState();
  const [owner_id, setOwner_id] = useState(null);
  const [cate, setCate] = useState(null);
  const [size, setSize] = useState();
  const [price, setPrice] = useState();
  const [description, setDescription] = useState();
  const [image, setImage] = useState();

  const handleOnclick = async () => {
    const formData = new FormData();
    formData.append("title", title);
    formData.append("owner_id", owner_id ? owner_id : userlist[0].user_id);
    formData.append("category", cate ? cate : cateList[0].category_id);
    formData.append("size", size ? size : "");
    formData.append("price", price ? price : "");
    formData.append("description", description);
    formData.append("image", image ? image : "");

    const data = await axios
      .post("http://localhost:8000/api/admin/product/create", formData)
      .then((response) => {
        setValue(true);
        if (response.data.success) {
          toast("success");
        } else {
          toast(response.data.message);
        }
      })
      .catch((error) => {
        if (error.response) {
          toast(error.response.data.message);
        } else {
          toast("Error");
        }
      });
  };

  return (
    <div className="add-product">
      <div className="add-product_layout">
        <h2>Add Product</h2>
        <span
          className="close-icon"
          onClick={() => {
            setOpenModal(false);
          }}
        >
          &times;
        </span>
        <div className="add-product_top">
          <div className="add-product_left">
            <input
              className="add-product__input"
              placeholder="Title"
              onChange={(e) => setTitle(e.target.value)}
            ></input>
            <select
              className="add-product__input"
              name="owner-id"
              id="id"
              onChange={(e) => setOwner_id(e.target.value)}
            >
              {userlist &&
                userlist.map((e) => {
                  return (
                    <option key={e.user_id} value={e.user_id}>
                      {e.name}
                    </option>
                  );
                })}
            </select>
            {/* <label for="cate">Select Category...</label> */}
            <select
              className="add-product__input"
              name="cate"
              id="cate"
              onChange={(e) => setCate(e.target.value)}
            >
              {cateList &&
                cateList.map((e) => {
                  return (
                    <option key={e.category_id} value={e.category_id}>
                      {e.name}
                    </option>
                  );
                })}
            </select>
            <div className="add-product__group">
              <input
                className="add-product__input-item"
                placeholder="Price"
                onChange={(e) => setPrice(e.target.value)}
              ></input>
              <input
                className="add-product__input-item"
                placeholder="size"
                onChange={(e) => setSize(e.target.value)}
              ></input>
            </div>
            <textarea
              className="add-product__short-description"
              placeholder="Short Description"
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
          </div>
          <div className="add-product_right">
            <img src={upload} className="add-product__img" alt="IMG"></img>

            <label for="file" className="add-product__label">
              UPLOAD PRODUCT IMAGE
              <input
                type="file"
                id="file"
                name=""
                className="add-product__file"
                hidden
                accept="image/png, image/jpeg"
                onChange={(e) => setImage(e.target.files[0])}
              ></input>
            </label>
          </div>
        </div>

        <textarea
          className="add-product__textarea"
          placeholder="Description"
        ></textarea>
        <div className="add-product__button-group">
          <button
            className="add-product__button-item pupble-color"
            onClick={() => handleOnclick()}
          >
            Add
          </button>
          <button
            className="add-product__button-item red-color"
            onClick={() => {
              setOpenModal(false);
            }}
          >
            Cancel
          </button>
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      {/* Same as */}
      <ToastContainer />
    </div>
  );
}

export default AddProduct;
