import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import { AuthContext } from "../../context/AuthContext";
// import {  Person } from 'react-bootstrap-icons';
// import { Lock } from 'react-bootstrap-icons';
import "./login.css";
import loginImg from "../../assets/images/login3.jpg";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useHistory } from "react-router-dom";
// import AlertMessage from '../layout/AlertMessage';

const Login = () => {
  let history = useHistory(); 
  const { loginUser } = useContext(AuthContext);

  const [loginForm, setLoginForm] = useState({
    email: "",
    password: "",
    is_remember: true,
  });

  const { email, password } = loginForm;

  const onChangeLoginForm = (event) =>
    setLoginForm({ ...loginForm, [event.target.name]: event.target.value });

  const login = async (event) => {
    event.preventDefault();

    try {
      const loginData = await loginUser(loginForm);
      if (loginData.success) {
        console.log(loginData.data);
        if (loginData.data.type_id === "ADM") {
          // history.push("/");
          window.location.href = "/";
        } else {
          toast("Does Not Have Access");
        }
      } else {
        toast(loginData.message);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="Login d-flex justify-content-center align-items-center">
      <div className="content-login">
        <div className="row h-100">
          <div className="col-md-6 h-100">
            <div className="h-100 d-flex justify-content-center align-items-center">
              <img className="img-fluid" src={loginImg} alt="login-img" />
            </div>
          </div>
          <div className="col-md-6 h-100">
            <div className="d-flex justify-content-center align-items-center w-100 h-100">
              <div className="login-form w-75 d-flex flex-column align-items-center">
                <div className="pb-3 d-flex justify-content-center">
                  <h4>Admin Login</h4>
                </div>
                <div>
                  <Form onSubmit={login}>
                    {alert ? alert.meessage : ""}
                    {/* {alert ? alert.message : ''} */}
                    {/* <AlertMessage info={alert}></AlertMessage> */}
                    <Form.Group className="mb-3 d-flex">
                      <div className="pl-3 ipnut-icon d-flex justify-content-center align-items-center">
                        {/* <Person></Person> */}
                      </div>
                      <Form.Control
                        className="input-custom"
                        name="email"
                        type="email"
                        placeholder="Email"
                        value={email}
                        onChange={onChangeLoginForm}
                        required
                      />
                    </Form.Group>
                    <Form.Group className="mb-3 d-flex">
                      <div className="pl-3 ipnut-icon d-flex justify-content-center align-items-center">
                        {/* <Lock></Lock> */}
                      </div>
                      <Form.Control
                        className="input-custom"
                        name="password"
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={onChangeLoginForm}
                        required
                      />
                    </Form.Group>
                    <Form.Group
                      className="mb-3 text-left"
                      controlId="formBasicCheckbox"
                    >
                      <Form.Check
                        className="ml-3"
                        type="checkbox"
                        label="Remember password"
                      />
                    </Form.Group>
                    <Button
                      variant="success"
                      className="btn-custom"
                      type="submit"
                    >
                      Login
                    </Button>
                    <Form.Group className="mb-3 text-left">
                      <Form.Label className="forgot">
                        <Link to="/forgot">Forgot Password</Link>
                      </Form.Label>
                    </Form.Group>
                  </Form>
                </div>
                <div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      {/* Same as */}
      <ToastContainer />
    </div>
  );
};

export default Login;
