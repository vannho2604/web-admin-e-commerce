import React, { useState } from "react";

import StatusCard from "../components/status-card/StatusCard";

import Chart from "react-apexcharts";

import { useSelector } from "react-redux";

// import { Link } from "react-router-dom";

import Table from "../components/table/Table";

import statusCards from "../assets/JsonData/status-card-data.json";

import salemanList from "../assets/JsonData/saleman-list.json";

import { DateTimePicker } from "react-rainbow-components";

import { Dropdown } from "react-bootstrap";

const chartOptions = {
  series: [
    {
      name: "Online Customers",
      data: [40, 70, 20, 90, 36, 80, 30, 91, 60, 65, 54, 77],
    },
    {
      name: "Store Customers",
      data: [40, 30, 70, 80, 40, 16, 40, 20, 51, 10, 34, 55],
    },
  ],
  options: {
    color: ["#6ab04c", "#2980b9"],
    chart: {
      background: "transparent",
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth",
    },
    xaxis: {
      categories: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ],
    },
    legend: {
      position: "top",
    },
    grid: {
      show: false,
    },
  },
};

const salemanTableHead = [
  "ID",
  "name",
  "email",
  "phone",
  "area",
  "revenue",
  "action",
];

const renderSalemanHead = (item, index) => <th key={index}>{item}</th>;

const renderSalemanBody = (item, index) => (
  <tr key={index}>
    <td>{item.id}</td>
    <td>{item.name}</td>
    <td>{item.email}</td>
    <td>{item.phone}</td>
    <td>{item.area}</td>
    <td>{item.revenue}</td>
    <td>
      <div className="view-action">
        <button>
          <i
            className="bx bx-show"
            style={{ fontSize: "20px", lineHeight: 1.5}}
          />
        </button>
      </div>
    </td>
  </tr>
);

const Dashboard = () => {
  const initialState = {
    value: new Date("2021-10-25 10:44"),
  };

  const [state, setState] = useState(initialState);
  const themeReducer = useSelector((state) => state.ThemeReducer.mode);

  const containerStyles = {
    maxWidth: 250,
  };

  return (
    <div>
      <h2 className="page-header">Dashboard</h2>
      <div className="row">
        <div className="col-6">
          <div className="row">
            {statusCards.map((item, index) => (
              <div className="col-6" key={index}>
                <StatusCard
                  icon={item.icon}
                  count={item.count}
                  title={item.title}
                />
              </div>
            ))}
          </div>
        </div>
        <div className="col-6">
          <div className="card full-height">
            {/* chart  */}
            <Chart
              options={
                themeReducer === "theme-mode-dark"
                  ? {
                      ...chartOptions.options,
                      theme: { mode: "dark" },
                    }
                  : {
                      ...chartOptions.options,
                      theme: { mode: "light" },
                    }
              }
              series={chartOptions.series}
              type="line"
              height="100%"
            />
          </div>
        </div>
        <div className="col-12">
          <div className="card">
            <div className="card__header">
              <h3>Saleman</h3>
              <div>
                <Dropdown>
                  <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select all area
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item href="/">Hai Chau</Dropdown.Item>
                    <Dropdown.Item href="/">Thanh Khe</Dropdown.Item>
                    <Dropdown.Item href="/">Son Tra</Dropdown.Item>
                    <Dropdown.Item href="/">Ngu Hanh Son</Dropdown.Item>
                    <Dropdown.Item href="/">Lien Chieu</Dropdown.Item>
                    <Dropdown.Item href="/">Cam Le</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
              <div>
                <div
                  className="rainbow-align-content_center rainbow-m-vertical_large rainbow-p-horizontal_small rainbow-m_auto"
                  style={containerStyles}
                >
                  <DateTimePicker
                    className="datetimepicker"
                    // label="DateTimePicker label"
                    value={state.value}
                    onChange={(value) => setState({ value })}
                    formatStyle="medium"
                    locale={"en-US"}
                    okLabel={"OK"}
                    cancelLabel={"Cancel"}
                  />
                </div>
              </div>
            </div>
            <div className="card__body">
              <Table
                limit="5"
                headData={salemanTableHead}
                renderHead={(item, index) => renderSalemanHead(item, index)}
                bodyData={salemanList}
                renderBody={(item, index) => renderSalemanBody(item, index)}
              />
            </div>
          </div>
        </div>
        {/* <div className="col-12">
                    <div className="card">
                        <div className="card__header">
                            <h3>Store</h3>
                            <div>                                                            
                                <div
                                    className="rainbow-align-content_center rainbow-m-vertical_large rainbow-p-horizontal_small rainbow-m_auto"
                                    style={containerStyles}
                                >
                                    <DateTimePicker
                                        className="datetimepicker"
                                        // label="DateTimePicker label"
                                        value={state.value}
                                        onChange={value => setState({ value })}
                                        formatStyle="medium"
                                        locale={"en-US"}
                                        okLabel={"OK"}
                                        cancelLabel={"Cancel"}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="card__body">
                            <Table
                                limit='5'
                                headData={storeTableHead}
                                renderHead={(item, index) => renderStoreHead(item, index)}
                                bodyData={storeList}
                                renderBody={(item, index) => renderStoreBody(item, index)}
                            />
                        </div>
                    </div>
                </div> */}
        {/* <div className="col-12">
                    <div className="card">
                        <div className="card__header">
                            <h3>Distributor</h3>
                            <div>                                                            
                                <div
                                    className="rainbow-align-content_center rainbow-m-vertical_large rainbow-p-horizontal_small rainbow-m_auto" 
                                    style={containerStyles}
                                >
                                    <DateTimePicker
                                        className="datetimepicker"
                                        // label="DateTimePicker label"
                                        value={state.value}
                                        onChange={value => setState({ value })}
                                        formatStyle="medium"
                                        locale={"en-US"}
                                        okLabel={"OK"}
                                        cancelLabel={"Cancel"}
                                    />
                                </div>                                
                            </div>                           
                        </div>
                        <div className="card__body">
                            <Table
                                limit='5'
                                headData={distributorTableHead}
                                renderHead={(item, index) => renderDistributorHead(item, index)}
                                bodyData={distributorList}
                                renderBody={(item, index) => renderDistributorBody(item, index)}
                            />
                        </div>
                    </div>
                </div> */}
      </div>
    </div>
  );
};

export default Dashboard;
