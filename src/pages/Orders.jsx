import React, { useState} from 'react'

import Table from '../components/table/Table'

import orderList from '../assets/JsonData/orders-list.json'

import { DateTimePicker } from 'react-rainbow-components'

import { Dropdown } from "react-bootstrap";

const orderTableHead = [
    'ID',
    'saleman',
    'shop',
    'total price',
    'area',
    'created_at',
    'updated_at',
    'status',
    'confirm',
    'action'
]

const renderHead = (item, index) => <th key={index}>{item}</th>

const renderBody = (item, index) => {

   let colorBtn = item.status
    return (<tr key={index}>
        <td>{item.id}</td>
        <td>{item.saleman}</td>
        <td>{item.shop}</td>
        <td>{item.total_price}</td>
        <td>{item.area}</td>
        <td>{item.created_at}</td>
        <td>{item.updated_at}</td>
        <td>
            <div className="status">
                <button className={`status-button color-${colorBtn}`}>{item.status}</button>
            </div>
        </td>
        <td>{item.enable ? <div style={{"color": "green"}}>Confirmed</div> : <div className="confirm"><button >Confirm</button></div> }</td>
        <td>
            <div className="view-action">
                {item.enable ? (
                    <button><i className="bx bx-show" style={{fontSize: "20px",lineHeight: 1.5}} /></button>
                ) : null}
            </div>
        </td>
    </tr>)
}


const Orders = () => {

    const initialState = {
        value: new Date('2021-10-25 10:44'),
        // locale: { name: 'en-US', label: 'English (US)' },
    };

    const [state, setState] = useState(initialState);

    const containerStyles = {
        maxWidth: 250,
    };

    return (
        <div>
            <h2 className="page-header">
                orders
            </h2>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="card__header">
                            <div>                                                            
                                <div
                                    className="rainbow-align-content_center rainbow-m-vertical_large rainbow-p-horizontal_small rainbow-m_auto"
                                    style={containerStyles}
                                >
                                    <DateTimePicker
                                        className="datetimepicker"
                                        // label="DateTimePicker label"
                                        value={state.value}
                                        onChange={value => setState({ value })}
                                        formatStyle="medium"
                                        locale={"en-US"}
                                        okLabel={"OK"}
                                        cancelLabel={"Cancel"}
                                    />
                                </div>
                            </div>
                            <div>
                                <Dropdown>
                                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                                    Select all bills
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item href="/">Hai Chau</Dropdown.Item>
                                    <Dropdown.Item href="/">Thanh Khe</Dropdown.Item>
                                    <Dropdown.Item href="/">Son Tra</Dropdown.Item>
                                    <Dropdown.Item href="/">Ngu Hanh Son</Dropdown.Item>
                                    <Dropdown.Item href="/">Lien Chieu</Dropdown.Item>
                                    <Dropdown.Item href="/">Cam Le</Dropdown.Item>
                                </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <div>
                                <Dropdown>
                                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                                    Select all area
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item href="/">Hai Chau</Dropdown.Item>
                                    <Dropdown.Item href="/">Thanh Khe</Dropdown.Item>
                                    <Dropdown.Item href="/">Son Tra</Dropdown.Item>
                                    <Dropdown.Item href="/">Ngu Hanh Son</Dropdown.Item>
                                    <Dropdown.Item href="/">Lien Chieu</Dropdown.Item>
                                    <Dropdown.Item href="/">Cam Le</Dropdown.Item>
                                </Dropdown.Menu>
                                </Dropdown>
                            </div>                            
                        </div>
                        <div className="card__body">
                            <Table
                                limit='5'
                                headData={orderTableHead}
                                renderHead={(item, index) => renderHead(item, index)}
                                bodyData={orderList}
                                renderBody={(item, index) => renderBody(item, index)}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Orders

