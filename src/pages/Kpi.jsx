import React, { useState, useEffect } from "react";

import axios from "axios";

import Table from "../components/table/Table";

import { Link } from "react-router-dom";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Kpi = () => {
  // const [kpi_id, setKpi_id] = useState();
  const [value, setValue] = useState(false);
  const [kpi, setKpi] = useState();
  const [a, setA] = useState();

  const kpiTableHead = ["ID", "name", "order_amount", "checkin", "action"];
  // const [value, setValue] = useState(false);

  const renderHead = (item, index) => <th key={index}>{item}</th>;

  async function fetchData() {
    const response = await axios.get(
      `http://localhost:8000/api/admin/kpi/list`
    );
        console.log('1111');
        setKpi(response.data.data);
    setValue(false);
  }

  async function deleteKpi(kpi_id) {
    await axios
      .post(`http://localhost:8000/api/admin/kpi/delete/${kpi_id}`)
      .then(async (response) => {
        if (response.data.success) {
            
            toast("success");
            await fetchData();
        //   const response = await axios.get(
        //     `http://localhost:8000/api/admin/kpi/list`
        //   );
        //   setKpi(kpi => ([...kpi, ...response.data.data]));
        } else {
          toast(response.data.message);
        }
      })
      .catch((error) => {
        if (error.response) {
          toast(error.response.data.message);
        } else {
          toast("Error");
        }
      });
  }

  const renderBody = (item, index) => {
    return (
      <tr>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.order_amount}</td>
        <td>{item.checkin}</td>
        <td>
          <div className="cate-action">
            <div className="delete-action">
              <button
                onClick={() => {
                  deleteKpi(item.id);
                }}
              >
                <i
                  className="bx bx-trash"
                  style={{ fontSize: "20px", lineHeight: 1.5 }}
                />
              </button>
            </div>
          </div>
        </td>
      </tr>
    );
  };



  useEffect(() => {
    fetchData();
  }, [value]);
  return (
    <div>
      <h2 className="page-header">KPI</h2>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card_header">
              <div className="add-new-cate">
                <button
                  href="./kpi_detail"
                  onClick={<Link to="./kpi_detail"></Link>}
                >
                  Add
                </button>
              </div>
            </div>
            <div className="card__body">
              {kpi ? (
                <Table
                  limit="5"
                  headData={kpiTableHead}
                  renderHead={(item, index) => renderHead(item, index)}
                  bodyData={kpi}
                  renderBody={(item, index) => renderBody(item, index)}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      {/* Same as */}
      <ToastContainer />
    </div>
  );
};

export default Kpi;
