import React, { useState, useEffect } from "react";

import axios from "axios";

import Table from "../components/table/Table";

import AddcateModal from "../components/modal/AddcateModal";

import EditcateModal from "../components/modal/EditcateModal";

import Row from "../components/Row";

import { apiUrl } from "../context/Constants";

const cateTableHead = ["ID", "name", "action"];

const renderHead = (item, index) => <th key={index}>{item}</th>;
const Category = () => {
  const [addmodalOpen, setaddmodalOpen] = useState(false);
  const [editmodalOpen, setEditmodalOpen] = useState(false);

  const [categories, setCategories] = useState();
  const [value, setValue] = useState(false);

  async function fetchData() {
    const response = await axios.get(`${apiUrl}/category/list`);

    setCategories(response.data.data);
    setValue(false);
  }

  useEffect(() => {
    fetchData();
  }, [value]);
  return (
    <>
      <div>
        <h2 className="page-header">Categories</h2>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card_header">
                <div className="add-new-cate">
                  <button
                    onClick={() => {
                      setaddmodalOpen(!addmodalOpen);
                    }}
                  >
                    Add new category
                  </button>
                </div>
              </div>
              <div className="card__body">
                {categories ? (
                <Table
                  limit="3"
                  headData={cateTableHead}
                  renderHead={(item, index) => renderHead(item, index)}
                  bodyData={categories}
                  renderBody={(item, index) => (
                    <Row
                      key={index}
                      item={item}
                      onEdit={() => {
                        setEditmodalOpen(true);
                      }}
                    />
                  )}
                />
                ) : null }
              </div>
            </div>
          </div>
        </div>
      </div>
      {addmodalOpen && <AddcateModal setOpenModal={setaddmodalOpen} />}
      {editmodalOpen && <EditcateModal setOpenModal={setEditmodalOpen} />}
    </>
  );
};

export default Category;
