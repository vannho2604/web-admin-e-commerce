import React from "react";

import Table from "../components/table/Table";

// import orderList from '../assets/JsonData/orders-list.json'

import orderdetailList from "../assets/JsonData/order-detail-list.json";
import '../assets/css/index.css'
const orderdetailTableHead = [
  "ID",
  "name product",
  "quantity",
  "distributor",
  "regular price",
];

const renderHead = (item, index) => <th key={index}>{item}</th>;

const renderBody = (item, index) => (
  <tr key={index}>
    <td>{item.id}</td>
    <td>{item.name_product}</td>
    <td>{item.quantity}</td>
    <td>{item.distributor}</td>
    <td>{item.regular_price}</td>
  </tr>
);
const Order_Detail = () => {
  return (
    <div>
      <h2 className="page-header">list orders : ID_Order: 3</h2>
      <div className="orderDetail__header">
        <p>Saleman<span className="orderDetail__header-title">DAARRAasads</span></p>
        <p>Address<span className="orderDetail__header-title">DAARRAasads</span></p>
        <p>Create_At<span className="orderDetail__header-title">DAARRAasads</span></p>
      </div>
      <div className="orderDetail__header">
        <p>Store<span className="orderDetail__header-title">DAARRAasads</span></p>
        <p>Phone<span className="orderDetail__header-title">DAARRAasads</span></p>
        <p>Total price<span className="orderDetail__header-title">DAARRAasads</span></p>
        <p>Status<span className="orderDetail__header-title">DAARRAasads</span></p>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card__body">
              <Table
                limit="5"
                headData={orderdetailTableHead}
                renderHead={(item, index) => renderHead(item, index)}
                bodyData={orderdetailList}
                renderBody={(item, index) => renderBody(item, index)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Order_Detail;
